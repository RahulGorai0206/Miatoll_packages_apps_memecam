PRODUCT_SOONG_NAMESPACES += \
    packages/apps/MiuiCamera

PRODUCT_COPY_FILES += \
    $(call find-copy-subdir-files,*,packages/apps/memecam/system/bin,$(TARGET_COPY_OUT_SYSTEM)/bin) \
    $(call find-copy-subdir-files,*,packages/apps/memecam/system/etc,$(TARGET_COPY_OUT_SYSTEM)/etc) \
    $(call find-copy-subdir-files,*,packages/apps/memecam/system/priv-app/MiuiCamera,$(TARGET_COPY_OUT_SYSTEM)/priv-app/MiuiCamera) \
    $(call find-copy-subdir-files,*,packages/apps/memecam/system/priv-app/MiuiScanner,$(TARGET_COPY_OUT_SYSTEM)/priv-app/MiuiScanner) \
    $(call find-copy-subdir-files,*,packages/apps/memecam/system/priv-app/MiuiExtraPhoto,$(TARGET_COPY_OUT_SYSTEM)/priv-app/MiuiExtraPhoto) \
    $(call find-copy-subdir-files,*,packages/apps/memecam/system/priv-app/MiuiScanner/lib/arm,$(TARGET_COPY_OUT_SYSTEM)/priv-app/MiuiScanner/lib/arm) \
    $(call find-copy-subdir-files,*,packages/apps/memecam/system/app/miuires,$(TARGET_COPY_OUT_SYSTEM)/app/miuires) \
    $(call find-copy-subdir-files,*,packages/apps/memecam/system/framework,$(TARGET_COPY_OUT_SYSTEM)/framework) \
    $(call find-copy-subdir-files,*,packages/apps/memecam/system/lib,$(TARGET_COPY_OUT_SYSTEM)/lib) \
    $(call find-copy-subdir-files,*,packages/apps/memecam/system/lib64,$(TARGET_COPY_OUT_SYSTEM)/lib64)

PRODUCT_PACKAGES += \
    MiuiCamera

PRODUCT_PRODUCT_PROPERTIES += \
    ro.miui.notch=0
    ro.com.google.lens.oem_camera_package=com.android.camera
    persist.vendor.camera.privapp.list=com.android.camera
    vendor.camera.aux.packagelist=com.android.camera

# Build
PRODUCT_BROKEN_VERIFY_USES_LIBRARIES := true
